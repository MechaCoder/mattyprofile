'use Strict';


var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
  return gulp.src('./a/src/sass/cust_style.scss')
    .pipe(
      sass().on('error', sass.logError)
    )
    .pipe(gulp.dest('./a/dist/css/'));
});

gulp.task('default', function(){
  gulp.watch('./a/src/sass/**/*.scss', ['sass']);
});

